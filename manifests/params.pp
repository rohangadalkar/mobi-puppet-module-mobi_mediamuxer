class mobi_mediamuxer::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 80 -u /mediamuxer/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $package = "mobi-mediamuxer"
    $version = "latest"
    $livebackend_fragmentserver=false
    $vodbackend_fragmentserver=false
    $recordingbackend_fragmentserver=false
    $licensemanagerserver=false
    $md3 = false
    # $serve_vod = "On"
    # $serve_recordings = "On"
    # $serve_live = "On"
    # $livecontentroot = "/var/www/dash-live"
    # $vodcontentroot = "/var/Jukebox/vod"
    # $recordingscontentroot = "/var/Jukebox/recordings"
    $deployment_config_path = "/usr/local/ods/mm/conf/deployment_config.json"
    $deployment_server_url = "http://stdps01p1:8080/deploymentserver"
    $drmproxy_url = "http://localhost/drmproxy"
    $vod_root_paths = false
    $live_root_paths = false
    $catchup_root_paths = false
    $serve_fmp4 = "On"
    $serve_dash = "On"
    $serve_hls = "On"
    $serve_ss = false
    $logfile_location = "/var/log/mobi-mediamuxer"
    $static_manifest_max_age = false
    $dynamic_manifest_max_age = false
    $vod_segment_max_age = false
    $live_segment_max_age = false
    $catchup_segment_max_age = false
    $subtitle_max_age = false
    $log_level = "6"
    $external_host_mm = false
}
