class mobi_mediamuxer::config {

    File {
        owner => root,
        group => root,
        mode => "0644",
    }
    $deployment_config_path = $mobi_mediamuxer::deployment_config_path
    $deployment_server_url = $mobi_mediamuxer::deployment_server_url
    $drmproxy_url = $mobi_mediamuxer::drmproxy_url
    $vod_root_paths = $mobi_mediamuxer::vod_root_paths
    $live_root_paths = $mobi_mediamuxer::live_root_paths
    $catchup_root_paths = $mobi_mediamuxer::catchup_root_paths
    $serve_fmp4 = $mobi_mediamuxer::serve_fmp4
    $serve_dash = $mobi_mediamuxer::serve_dash
    $serve_hls = $mobi_mediamuxer::serve_hls
    $serve_ss = $mobi_mediamuxer::serve_ss
    $static_manifest_max_age = $mobi_mediamuxer::static_manifest_max_age
    $dynamic_manifest_max_age = $mobi_mediamuxer::dynamic_manifest_max_age
    $vod_segment_max_age = $mobi_mediamuxer::vod_segment_max_age
    $live_segment_max_age = $mobi_mediamuxer::live_segment_max_age
    $catchup_segment_max_age = $mobi_mediamuxer::catchup_segment_max_age
    $subtitle_max_age = $mobi_mediamuxer::subtitle_max_age
    $log_level = $mobi_mediamuxer::log_level
    $external_host_mm = $mobi_mediamuxer::external_host_mm

    if (!$deployment_config_path){ fail("Mandatory parameter not set!")}
    if (!$deployment_server_url){ fail("Mandatory parameter not set!")}
    if (!$serve_fmp4){ fail("Mandatory parameter not set!")}
    if (!$serve_dash){ fail("Mandatory parameter not set!")}
    if (!$serve_hls){ fail("Mandatory parameter not set!")}
    if (!$serve_ss){ fail("Mandatory parameter not set!")}

    file { "/etc/httpd/conf.d/mod_mm.conf":
            ensure => present,
            content => template("mobi_mediamuxer/mod_mm.conf.md3.erb"),
            notify => Service["httpd"],
            require => Class["apache::install"],
    }

    file { "/etc/httpd/conf.d/md_access.conf":
            ensure => present,
            content => template("mobi_mediamuxer/md_access.conf.erb"),
            notify => Service["httpd"],
            require => Class["apache::install"],
    }

    file { "${mobi_mediamuxer::logfile_location}":
            ensure  => directory,
            owner   => "apache",
            group   => "apache",
            mode    => "0775",
            require => Class["mobi_mediamuxer::install"],
     }
     # Add mediamuxer rsyslog configuation
     if defined(Class["rsyslog"]) {
         file { "/etc/rsyslog.d/mediamuxer.conf":
              ensure => present,
              owner => 'root',
              group => 'root',
              mode => '0644',
              notify => Class["rsyslog::service"],
              source => "puppet:///modules/mobi_mediamuxer/rsyslog/mediamuxer.conf",
          }
     }

     file { "/etc/logrotate.d/mobi_mediamuxer":
           source => "puppet:///modules/mobi_mediamuxer/mobi_mediamuxer.logrotate",
           owner => 'root',
           group => 'root',
     }

    file { "/var/www/html/crossdomain.xml":
           source => "puppet:///modules/mobi_mediamuxer/crossdomain.xml",
           owner => 'root',
           group => 'root',
     }
 
     file { "/etc/httpd/conf.d/mobi_mm_cors.conf":
           source => "puppet:///modules/mobi_mediamuxer/mobi_mm_cors.conf",
           owner => 'root',
           group => 'root',
     }
}
